-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Machine: 127.0.0.1
-- Gegenereerd op: 25 mrt 2015 om 11:13
-- Serverversie: 5.6.16
-- PHP-versie: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Databank: `php_videotheek`
--
CREATE DATABASE IF NOT EXISTS `php_videotheek` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `php_videotheek`;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `dvd`
--

DROP TABLE IF EXISTS `dvd`;
CREATE TABLE IF NOT EXISTS `dvd` (
  `nummerId` int(11) NOT NULL,
  `titelId` int(11) NOT NULL,
  `aanwezig` bit(1) NOT NULL DEFAULT b'1',
  UNIQUE KEY `nummerId` (`nummerId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Tabel leegmaken voor invoegen `dvd`
--

TRUNCATE TABLE `dvd`;
--
-- Gegevens worden geëxporteerd voor tabel `dvd`
--

INSERT INTO `dvd` (`nummerId`, `titelId`, `aanwezig`) VALUES
(1, 1, b'0'),
(2, 10, b'1'),
(3, 8, b'1'),
(4, 8, b'0'),
(5, 46, b'0'),
(6, 46, b'1'),
(54, 17, b'1'),
(55, 15, b'1'),
(65, 41, b'0'),
(66, 41, b'1'),
(85, 15, b'1'),
(98, 41, b'0'),
(113, 15, b'1'),
(120, 16, b'0'),
(121, 16, b'1'),
(123, 16, b'1'),
(141, 16, b'0'),
(187, 17, b'0'),
(188, 17, b'1');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `titel`
--

DROP TABLE IF EXISTS `titel`;
CREATE TABLE IF NOT EXISTS `titel` (
  `titelId` int(11) NOT NULL AUTO_INCREMENT,
  `titel` varchar(50) NOT NULL,
  PRIMARY KEY (`titelId`),
  UNIQUE KEY `titel` (`titel`),
  FULLTEXT KEY `titel_2` (`titel`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=47 ;

--
-- Tabel leegmaken voor invoegen `titel`
--

TRUNCATE TABLE `titel`;
--
-- Gegevens worden geëxporteerd voor tabel `titel`
--

INSERT INTO `titel` (`titelId`, `titel`) VALUES
(10, '22 Jump Street'),
(11, 'Boyhood'),
(41, 'Darkest Hour'),
(46, 'Descendants, The'),
(44, 'Facebook'),
(9, 'Forrest Gump'),
(43, 'Girl With The Dragon Tattoo'),
(3, 'Green Mile, The'),
(6, 'Lord of the Rings: The Return of the King, The'),
(5, 'Once Upon a Time in the West'),
(4, 'Pulp Fiction'),
(17, 'Safe House'),
(8, 'Schindler''s List'),
(7, 'Se7en'),
(1, 'Shawshank Redemption, The'),
(12, 'Tammy'),
(16, 'Trespass'),
(14, 'Vermist');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
