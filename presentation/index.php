<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Videotheek - Hoofdmenu</title>
        <link rel="stylesheet" href="css/videotheek.css" type="text/css">
    </head>
    <body>
        <h1>
            Hoofdmenu
        </h1>
        <?php
        if(isset($uitgevoerd)){
            ?>
            <div>
                <?php print($uitgevoerd);?>
            </div>
        <?php
        }
        ?>
        <?php
        if(isset($error)){
            ?>
            <div class="fout">
                <?php print($error);?>
            </div>
        <?php
        }
        ?>
        <table>
            <tr>
                <td>
                    <a href="index.php?actie=overzicht">Overzicht</a>
                </td>
                <td>
                    <a href="index.php?actie=zoekdvdopnummer">Zoek DVD op nummer</a>
                </td>
                <td>
                    <a href="index.php?actie=invoegentitel">Nieuwe titel invoegen</a>
                </td>
            </tr>
            <tr>
                <td>
                    <a href="index.php?actie=invoegendvd">Nieuwe DVD invoegen</a>
                </td>
                <td>
                    <a href="index.php?actie=verwijdertitel">Verwijder titel</a>
                </td>
                <td></td>
            </tr>
            <tr>
                <td>
                    <a href="index.php?actie=verwijderdvd">Verwijder DVD</a>
                </td>
                <td>
                    <a href="index.php?actie=huurdvd">Huur DVD</a>
                </td>
                <td>
                    <a href="index.php?actie=brengterugdvd">DVD terugbrengen</a>
                </td>
            </tr>
        </table>
    </body>
</html>
